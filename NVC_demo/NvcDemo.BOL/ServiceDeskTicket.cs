﻿using System;

namespace NvcDemo.BOL
{
    [Serializable]
    public class ServiceDeskTicket
    {
        public ServiceDeskTicket()
        {

        }
        public ServiceDeskTicket(int id)
        {
            Id = id;
        }
        public int Id { get; private set; }
        public Employee RequestedBy { get; set; }
        public Employee RequestedFor { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public TicketCategory IssueType { get; set; }
    }

    public enum TicketCategory
    {
        CaseProcessing,
        Credentials,
        ComputerHardware,
        ComputerSoftware
    }
}
