﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace NvcDemo.BOL
{
    [Serializable]
    public class BusinessUnit
    {
        public BusinessUnit()
        {

        }
        public BusinessUnit(int id, string moniker)
        {
            Id = id;
            Moniker = moniker;
        }

        [XmlAttribute]
        public int Id { get; set; }
        [XmlAttribute]
        public string Name { get; set; }
        [XmlAttribute]
        public string Moniker { get; set; }
        
        public Employee Manager { get; set; }
        public List<Employee> Associates { get; set; }
    }
}
