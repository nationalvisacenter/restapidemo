﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace NvcDemo.BOL
{
    [Serializable]
    public class Employee
    {
        public Employee()
        {

        }
        public Employee(string lanId)
        {
            LanId = lanId;
            Role = EmployeeRole.Associate;
        }
        [XmlAttribute]
        public string LanId { get; set; }
        [XmlAttribute]
        public string FirstName { get; set; }
        [XmlAttribute]
        public string LastName { get; set; }
        public BusinessUnit Unit { get; set; }
        [XmlAttribute]
        public EmployeeRole Role { get; set; }

    }

    public enum EmployeeRole
    {
        Associate,
        CustomerServiceAnalyst,
        Manager,
        SupportStaff,
        Other
    }
}
