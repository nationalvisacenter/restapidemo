﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using NvcDemo.BOL;

namespace NvcDemo.DAL
{
    public class ServiceDeskDemoData : IServiceDeskData
    {
        private readonly DemoData _db;

        public ServiceDeskDemoData()
        {
            //to-do: check for existence of xml DB and create if not
            _db = new DemoData();
            //saveDemoDataToXml(_db);
        }
        public ServiceDeskTicket GetTicket(int Id)
        {
            throw new NotImplementedException();
        }

        public ICollection<ServiceDeskTicket> GetTickets(bool includeDetails)
        {
            return _db.Tickets;
        }

        private void saveDemoDataToXml(DemoData data)
        {
            throw new NotImplementedException();
            //DemoData dat = new DemoData();
            //System.Xml.Serialization.XmlSerializer writer =
            //new System.Xml.Serialization.XmlSerializer(typeof(DemoData));

            //var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "//WebApiDemoData.xml";
            //System.IO.FileStream file = System.IO.File.Create(path);

            //writer.Serialize(file, data);
        }
    }

    [Serializable]
    public class DemoData
    {
        //private const int ticketCount = 100;
        protected static int testTicketId = 100;
        protected static int testUnitId = 11;

        
        public ICollection<ServiceDeskTicket> Tickets
        {
            get
            {
                var toRet = new List<ServiceDeskTicket>();
                toRet.Add(new ServiceDeskTicket(testTicketId++)
                {
                    RequestedBy = Employees.Where(e => e.LanId == "johnsonmr").FirstOrDefault(),
                    RequestedFor = Employees.Where(e => e.LanId == "jansenbb").FirstOrDefault(),
                    Title = "Need Password Reset",
                    Description = "Need Password Reset",
                    IssueType = TicketCategory.Credentials
                });
                toRet.Add(new ServiceDeskTicket(testTicketId++)
                {
                    RequestedBy = Employees.Where(e => e.LanId == "sorentome").FirstOrDefault(),
                    RequestedFor = Employees.Where(e => e.LanId == "delimoshw").FirstOrDefault(),
                    Title = "Need Password Reset",
                    Description = "Need Password Reset",
                    IssueType = TicketCategory.Credentials
                });
                toRet.Add(new ServiceDeskTicket(testTicketId++)
                {
                    RequestedBy = Employees.Where(e => e.LanId == "jonesrj").FirstOrDefault(),
                    RequestedFor = Employees.Where(e => e.LanId == "jansenbb").FirstOrDefault(),
                    Title = "Need new monitor",
                    Description = "Computer monitor is not working",
                    IssueType = TicketCategory.ComputerHardware
                });
                //for (int i=1; i<=ticketCount; i++)
                //{

                //}
                return toRet;
            }
        }

        [XmlArray]
        public Employee[] Employees
        {
            get {
                var toRet = new Employee[]
                {
                    //--DR
                    
                    new Employee("johnsonmr")
                    {
                        FirstName="Mike",
                        LastName="Johnson",
                        Unit=Units.Where(u=>u.Moniker=="DR").First(),
                        Role=EmployeeRole.Manager
                    },
                    new Employee("jonesrj")
                    {
                        FirstName="Robert",
                        LastName="Jones",
                        Unit=Units.Where(u=>u.Moniker=="DR").First(),
                        Role=EmployeeRole.CustomerServiceAnalyst
                    },
                    new Employee("jansenbb")
                    {
                        FirstName="Billy",
                        LastName="Jansen",
                        Unit=Units.Where(u=>u.Moniker=="DR").First(),
                    },
                    //--WC
                    new Employee("sorentome")
                    {
                        FirstName="Sorento",
                        LastName="Mel",
                        Unit=Units.Where(u=>u.Moniker=="WC").First(),
                        Role=EmployeeRole.Manager
                    },
                    new Employee("delimoshw")
                    {
                        FirstName="Delinos",
                        LastName="Hethens",
                        Unit=Units.Where(u=>u.Moniker=="WC").First(),
                    },
                };
                return toRet;
            }

        }
        
        public List<BusinessUnit> Units
        {
            get
            {
                var toRet = new List<BusinessUnit>
                {
                    new BusinessUnit(testUnitId++, "DR")
                    {
                        Name="Document Review",
                    },
                    new BusinessUnit(testUnitId++, "WC")
                    {
                        Name="Written Correspondence"
                    },
                    
                };

                return toRet;
            }
        }
    }

}
