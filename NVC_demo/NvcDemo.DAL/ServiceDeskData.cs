﻿using NvcDemo.BOL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NvcDemo.DAL
{
    public class ServiceDeskData : IServiceDeskData
    {
        public ServiceDeskTicket GetTicket(int Id)
        {
            throw new NotImplementedException();
        }

        public ICollection<ServiceDeskTicket> GetTickets(bool includeDetails)
        {
            throw new NotImplementedException();
        }
    }

    public interface IServiceDeskData
    {
        ServiceDeskTicket GetTicket(int Id);
        ICollection<ServiceDeskTicket> GetTickets(bool includeDetails);
    }
}
