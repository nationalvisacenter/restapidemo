﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NvcDemo.BOL;
using NvcDemo.DAL;
using System;
using System.Linq;

namespace Nvc_demo.Web_REST.Controllers
{
    /// <summary>
    /// represents the controller to handler all requests for NVC ServiceDesk Tickets.    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class TicketsController : ControllerBase
    {
        private IServiceDeskData _data;
        public TicketsController(IServiceDeskData data)
        {
            _data = data;
        }
        [HttpGet]
        public ActionResult<ServiceDeskTicket[]> GetTickets(bool includeDetails = false)
        {
            try
            {
                var model = _data.GetTickets(includeDetails);
                return model.ToArray();
            }
            catch
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Something went wrong... too bad");
            }
        }
    }
}